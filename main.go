package main

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"

	"github.com/gin-gonic/gin"
)

// A Moderation captures the interesting detail about one action
type Moderation struct {
	CreatedAt      string `json:"created_at"`
	Action         string `json:"action"`
	Reason         string `json:"reason"`
	Moderator      string `json:"moderator"`
	Story          string `json:"story"`
	Comment        string `json:"comment"`
	User           string `json:"user"`
	FromSuggestion bool   `json:"fromSuggestion"`
}

func main() {
	user := os.Getenv("DB_USERNAME")
	password := os.Getenv("DB_PASSWORD")
	dbhost := os.Getenv("DB_HOST")
	dbname := os.Getenv("DB_NAME")
	connstr := fmt.Sprintf("%s:%s@tcp(%s)/%s", user, password, dbhost, dbname)

	fmt.Println(connstr)

	db, err := sql.Open("mysql", connstr)

	if err != nil {
		panic(err)
	}

	defer db.Close()

	router := gin.Default()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	router.GET("/", func(c *gin.Context) {
		before := c.Query("before")

		var rows *sql.Rows

		if len(before) > 0 {
			rows, err = db.Query(
				"SELECT m.created_at, moderator.username, story.title, comment.comment, user.username, m.action, m.reason, m.is_from_suggestions "+
					"FROM moderations m "+
					"     LEFT JOIN users moderator  ON moderator.id = m.moderator_user_id "+
					"     LEFT JOIN users user       ON user.id      = m.user_id "+
					"     LEFT JOIN stories story    ON story.id     = m.story_id "+
					"     LEFT JOIN comments comment ON comment.id   = m.comment_id "+
					"WHERE DATEDIFF(m.created_at, ?) < 0 "+
					"ORDER BY m.created_at DESC "+
					"LIMIT 50", before)
		} else {
			rows, err = db.Query(
				"SELECT m.created_at, moderator.username, story.title, comment.comment, user.username, m.action, m.reason, m.is_from_suggestions " +
					"FROM moderations m " +
					"     LEFT JOIN users moderator  ON moderator.id = m.moderator_user_id " +
					"     LEFT JOIN users user       ON user.id      = m.user_id " +
					"     LEFT JOIN stories story    ON story.id     = m.story_id " +
					"     LEFT JOIN comments comment ON comment.id   = m.comment_id " +
					"ORDER BY m.created_at DESC " +
					"LIMIT 50")
		}

		if err != nil {
			c.Error(err)
			c.String(400, "Errors found %v", c.Errors)
			return
		}

		defer rows.Close()

		moderations := make([]Moderation, 0)

		for rows.Next() {
			var createdAt, action, reason sql.NullString
			var moderator, story, comment, user sql.NullString
			var fromSuggestion bool

			if err := rows.Scan(&createdAt, &moderator, &story, &comment, &user, &action, &reason, &fromSuggestion); err != nil {
				c.Error(err)
			} else {
				createdAt := validOrEmptyS(createdAt)
				action := validOrEmptyS(action)
				reason := validOrEmptyS(reason)
				moderator := validOrEmptyS(moderator)
				story := validOrEmptyS(story)
				comment := validOrEmptyS(comment)
				user := validOrEmptyS(user)
				moderations = append(moderations, Moderation{createdAt, action, reason, moderator, story, comment, user, fromSuggestion})
			}
		}

		if err := rows.Err(); err != nil {
			c.Error(err)
		}

		if len(c.Errors) == 0 {
			c.JSON(200, moderations)
		} else {
			c.String(400, "Errors found %v", c.Errors)
		}
	})
	router.Run(":1972")
}

func validOrEmptyS(s sql.NullString) string {
	if s.Valid {
		return s.String
	} else {
		return ""
	}
}

func validOrEmptyI(i sql.NullInt64) int64 {
	if i.Valid {
		return i.Int64
	} else {
		return 0
	}
}
