build: dependencies
    env GOOS=linux go build -ldflags="-s -w" -o m2m.modlog main.go
dependencies:
    go get github.com/go-sql-driver/mysql
    go get github.com/gin-gonic/gin
lint: dependencies
    go get github.com/alecthomas/gometalinter
    gometalinter --enable-all ./...
test: dependencies
    go test -v ./...
