FROM golang:1.10

WORKDIR /app
COPY m2m.modlog /app

EXPOSE 1972

CMD ["/app/m2m.modlog"]
